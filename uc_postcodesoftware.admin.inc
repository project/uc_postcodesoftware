<?php

/**
 * Settings form.
 */
function uc_postcodesoftware_settings_form() {
  $form['uc_postcodesoftware_test'] = array(
    '#type' => 'checkbox',
    '#title' => t('Test mode'),
    '#description' => 'You do not need a PostcodeSoftware account for test mode, but lookups will only work for postcodes beginning with LS18.',
    '#default_value' => variable_get('uc_postcodesoftware_test', FALSE),
  );
  $form['uc_postcodesoftware_account'] = array(
    '#type' => 'textfield',
    '#title' => t('Account'),
    '#size' => 20,
    '#default_value' => variable_get('uc_postcodesoftware_account', ''),
  );
  $form['uc_postcodesoftware_password'] = array(
    '#type' => 'textfield',
    '#title' => t('Password'),
    '#size' => 20,
    '#default_value' => variable_get('uc_postcodesoftware_password', ''),
  );

  return system_settings_form($form);
}
