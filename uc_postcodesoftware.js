
function addrfield(type, name) {
  return $('#edit-panes-' + type + '-' + type + '-' + name);
}

$(function() {
  $('.postcodesoftware-find').click(function() {
    var button = $(this);
    var field = button.parent().prev();
    var type = field.attr('id').split('-')[2];
    var postcode = field.val().toUpperCase().replace(/\s/g, '');
    if (!postcode.length) {
      alert('Please enter a valid postcode.');
    }
    else {
      var country = addrfield(type, 'country');
      if (country.val() != 826) {
        country.val(826).trigger('change');
      }
      $.post(Drupal.settings.basePath + 'cart/postcodesoftware', { postcode: postcode }, function(data) {
        if (data.error && data.error.length) {
          alert(data.error);
        } else {
          var options = '<option value="">Select the first line of your address:</option>';
          for (var i = 0; i < data.premises.length; i++) {
            options += '<option value="'+ i +'">'+ data.premises[i].street1 +' '+ data.street1 +'</option>';
          }

          $('.find-premises').remove();
          button.after(' <select class="find-premises">'+ options + '</select>').next().change(function() {
            var street = data.premises[$(this).val()].street1 +' '+ data.street1;
            addrfield(type, 'street1').val(street).trigger('change');
            addrfield(type, 'street2').val(data.street2).trigger('change');
            addrfield(type, 'city').val(data.city).trigger('change');
            addrfield(type, 'postal-code').val(data.postal_code).trigger('change');
            addrfield(type, 'zone').val(data.zone).trigger('change');
          });
        }
      }, 'json');
    }
    return false;
  });
});
