<?php

/**
 * Look up a postcode and return address data as JSON.
 *
 * Based on code from http://www.postcodesoftware.co.uk/download/php4_ajax_example.zip
 */
function uc_postcodesoftware_lookup() {
  $account = variable_get('uc_postcodesoftware_account', '');
  $password = variable_get('uc_postcodesoftware_password', '');
  if (variable_get('uc_postcodesoftware_test', FALSE)) {
    $account = $password = 'test';
  }

  $postcode = str_replace(' ', '', $_POST['postcode']);
  if ($cached = cache_get('uc_postcodesoftware:' . $postcode)) {
    $data = $cached->data;
  }
  else {
    $url = "http://ws1.postcodesoftware.co.uk/lookup.asmx/getAddress?account=$account&password=$password&postcode=$postcode";

    $map = array(
      'Address1' => 'street1',
      'Address2' => 'street2',
      'Town' => 'city',
      'County' => 'zone',
      'Postcode' => 'postal_code',
      'PremiseData' => 'premise_data',
      'ErrorMessage' => 'error',
    );

    foreach ($map as $field) {
      $data[$field] = '';
    }

    // TODO: make this more robust, or switch to PHP5 SimpleXML
    $xml = @fopen($url, 'r');
    if ($xml) {
      while (!feof($xml)) {
        $buffer = trim(fgets($xml, 4096)); // Retrieves the element and removes spaces either side of the element
        $position = strpos(substr($buffer, 1), ">"); // Find the position where the '>' is on the first tag
        $lstposition = strpos(substr($buffer, $position), "<"); // Find the position of the '<' on the last tag
        $xmltag = substr($buffer, 1, $position); // Gets the xml tag name
        $xmldata = substr($buffer, $position + 2, $lstposition - 2); // Gets the xml value

        if (isset($map[$xmltag])) {
          $data[$map[$xmltag]] = $xmldata;
        }
      }
    }
    fclose($xml);

    // convert zone name to id if possible
    $zone = db_result(db_query("SELECT zone_id FROM {uc_zones} WHERE zone_country_id = 826 AND zone_name = '%s'", $data['zone']));
    if ($zone) {
      $data['zone'] = $zone;
    }
    else {
      watchdog('uc_postcodesoftware', 'Unknown zone for county: @county', array('@county' => $data['zone']), WATCHDOG_WARNING);
    }

    // convert premises list
    $premises = split(';', $data['premise_data']);
    unset($data['premise_data']);
    $data['premises'] = array();
    foreach ($premises as $premise) {
      if (!$premise) continue;

      list($organisation, $building, $number) = split('[|]', $premise);
      if ($building) {
        $building = str_replace('/', ', ', $building.'/');
      }

      $data['premises'][] = array(
        'company' => trim($organisation),
        'street1' => $building . $number,
      );
    }

    cache_set('uc_postcodesoftware:'. $postcode, $data);
  }

  drupal_set_header("Content-Type: text/javascript; charset=utf-8");
  print drupal_to_js($data);
}
